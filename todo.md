This file contains all the stuff I might need to solve urgently, or in due course as the project goes somewhere.

1. Enable WandB integration (including account creation etc)
1. Link Readme to WandB
1. Link Readme to Notion
1. Start documenting assumptions and work done so far
1. Add logging

Existing ideas:

1. **Alternate Graph Formulation**

    1. graph nodes are lemmas:
    edge exists between `la` and `lb` where `la in synseta` and `lb in synsetb` IF
    `synseta` and `synsetb` are related with some edge
    
    1. edge has hyperrelational anntoations:
    `domainsynset: synseta`;
    `rangesynset: synsetb`
