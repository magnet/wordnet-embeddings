
<h1 align="center">
  wordnet-embeddings
</h1>

## Features
* Relational Graph Convolutional Networks (CompGCN)
* Hyper-relational Graph Convolutional Networks (StarE)
* Create wordnet synset-synset graph
* Simple NLP tasks on which to evaluate these wordnet embeddings 

# Set Up

## Requirements
* Python 3.8
* PyTorch 1.5.1
* torch-geometric 1.6.1
* torch-scatter 2.0.5
* tqdm
* wandb
* nltk

## Set up

Create a new conda environment and execute `setup.sh`.
Alternatively
```
pip install -r requirements.txt
```

TODO: also mention how do download NLTK data

## WandB Login

If you're logging your runs on WandB, you need to do login. 
Do this, just once run 
```bash
    wandb login yourkeyyourkeyyourkeyyourkeyyourkeyyourk
```

It would then generate a `~/.netrc` file which is going to be pulled up in subsequent runs.

If your runs are run through a container, 
    and you would rather not have to interactively login at all times,
    go to your `~/.bashrc` and add

```bash
    WANDB_API_KEY=$YOUR_API_KEY
```

Make sure that this `*.rc` file is sourced in your new environment and you should be good to go.

## Running Common Scripts

### CompGCN over synset-synset WordNet relational graph

```bash
python src/run.py SAMPLER_W_QUALIFIERS False DEVICE cuda SAVE True EVAL_EVERY 4 EPOCHS 100 DATASET wn_synset MODEL_NAME stare_transformer
```

Crucially, `DATASET` indicates which wordnet graph are we targetting here,
`MODEL_NAME` indicates the encoder (StarE), and decoder (Transformer) we're using to train the model,
`SAMPLE_W_QUALIFIERS` when set to false indicates that we're working over relational and not hyper relational graph. 

### + node embeddings initialized with word2vec
```WN_INITIAL wordtovec-gloss EMBEDDING_DIM 300 GCN_GCN_DIM 300```

Why the last two: Keep in mind that the embedding dimensions must match the predefined embedding dims (default: 200).
So if we're using 300D wordtovec to init node embeddings, you must also pass the emb dim. 
TODO: maybe automate this?

 
### + resume training from a specifically saved model
``` RESUME True RESUME_NM 4```

Here, `RESUME True` indicates that we want the model to load the model and optimizer state dict before starting to train, in effect resuming the training.
If **no `RESUME_NM` is specified**, the loaded model would be `./models/{DATASET}/{MODEL_NAME}/{the last number}`. Set `RESUME_NM` to specify "which number".


### + logging results on WandB
```WANDB True```


# Rough


## Running Experiments

### Available models
Specified as `MODEL_NAME` in the running script
* `stare_transformer` - main model StarE (H) + Transformer (H) [default]
* `stare_stats_baseline` - baseline model Transformer (H)
* `stare_trans_baseline` - baseline model Transformer (T)

### Datasets
Specified as `DATASET` in the running script
* `jf17k`
* `wikipeople`
* `wd50k` [default]
* `wd50k_33` 
* `wd50k_66`
* `wd50k_100`

### Starting training and evaluation
It is advised to run experiments on a GPU otherwise training might take long.
Use `DEVICE cuda` to turn on GPU support, default is `cpu`.
Don't forget to specify `CUDA_VISIBLE_DEVICES` before `python` if you use `cuda`

Three parameters control triple/hyper-relational nature and max fact length:
* `STATEMENT_LEN`: `-1` for hyper-relational [default], `3` for triples
* `MAX_QPAIRS`: max fact length (3+2*quals), e.g., `15` denotes a fact with 5 qualifiers `3+2*5=15`.
`15` is default for `wd50k` datasets and `jf17k`, set `7` for wikipeople, set `3` for triples (in combination with `STATEMENT_LEN 3`) 
* `SAMPLER_W_QUALIFIERS`: `True` for hyper-relational models [default], `False` for triple-based models only 

The following scripts will train StarE (H) + Transformer (H) for 400 epochs and evaluate on the test set:

* StarE (H) + Transformer (H)
```
python run.py DATASET wd50k
```  
* StarE (H) + Transformer (H) with a GPU.
```
CUDA_VISIBLE_DEVICES=0 python run.py DEVICE cuda DATASET wd50k
``` 
*  You can adjust the dataset with a higher ratio of quals by changing `DATASET` with the available above names
```
python run.py DATASET wd50k_33
```
* On JF17K
```
python run.py DATASET jf17k CLEANED_DATASET False
```
* On WikiPeople
```
python run.py DATASET wikipeople CLEANED_DATASET False MAX_QPAIRS 7 EPOCHS 500
```

Triple-based models can be started with this basic set of params:
```
python run.py DATASET wd50k STATEMENT_LEN 3 MAX_QPAIRS 3 SAMPLER_W_QUALIFIERS False
```

More hyperparams are available in the `CONFIG` dictionary in the `run.py`.

If you want to adjust StarE encoder params prepend `GCN_` to the params in the `GCNARGS` dict, e.g., 
```
python run.py DATASET wd50k GCN_GCN_DIM 80 GCN_QUAL_AGGREGATE concat
```
will construct StarE with hidden dim of 80 and concat as `gamma` function from the paper.

### Integration with Weights & Biases (WANDB)

It's there out of the box! Create an account on [WANDB](https://wandb.ai)
Then, make sure you install the latest version of the package
```
pip install wandb
```

Locate your API_KEY in the user settings and activate it:
```
wandb login <api_key>
```

Then just use the CLI argument `WANDB True`, it will:
* Create a `wikidata-embeddings` project in your active team
* Create a run with a random name and log results there


#### When using this codebase or dataset please cite:

```
@inproceedings{StarE,
  title={Message Passing for Hyper-Relational Knowledge Graphs},
  author={Galkin, Mikhail and Trivedi, Priyansh and Maheshwari, Gaurav and Usbeck, Ricardo and Lehmann, Jens},
  booktitle={EMNLP},
  year={2020}
}
```

For any further questions, please contact:  ```mikhail.galkin@iais.fraunhofer.de```
