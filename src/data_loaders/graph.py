"""
    File which enables easily loading any dataset we need
"""
import json
import pickle
import random
from pathlib import Path
from typing import List, Dict, Union

import numpy as np
from tqdm import tqdm

from config import LOCATIONS as LOC


def _conv_to_our_format_(data, filter_literals=True):
    conv_data = []
    dropped_statements = 0
    dropped_quals = 0
    for datum in tqdm(data):
        try:
            conv_datum = []

            # Get head and tail rels
            head, tail, rel_h, rel_t = None, None, None, None
            for rel, val in datum.items():
                if rel[-2:] == '_h' and type(val) is str:
                    head = val
                    rel_h = rel[:-2]
                if rel[-2:] == '_t' and type(val) is str:
                    tail = val
                    rel_t = rel[:-2]
                    if filter_literals and "http://" in tail:
                        dropped_statements += 1
                        raise Exception

            assert head and tail and rel_h and rel_t, f"Weird data point. Some essentials not found. Quitting\nD:{datum}"
            assert rel_h == rel_t, f"Weird data point. Head and Tail rels are different. Quitting\nD: {datum}"

            # Drop this bs
            datum.pop(rel_h + '_h')
            datum.pop(rel_t + '_t')
            datum.pop('N')
            conv_datum += [head, rel_h, tail]

            # Get all qualifiers
            for k, v in datum.items():
                for _v in v:
                    if filter_literals and "http://" in _v:
                        dropped_quals += 1
                        continue
                    conv_datum += [k, _v]

            conv_data.append(tuple(conv_datum))
        except Exception:
            continue
    print(
        f"\n Dropped {dropped_statements} statements and {dropped_quals} quals with literals \n ")
    return conv_data


def _conv_to_our_quint_format_(data, filter_literals=True):
    conv_data = []
    dropped_statements = 0
    dropped_quals = 0
    for datum in tqdm(data):
        try:
            conv_datum = []

            # Get head and tail rels
            head, tail, rel_h, rel_t = None, None, None, None
            for rel, val in datum.items():
                if rel[-2:] == '_h' and type(val) is str:
                    head = val
                    rel_h = rel[:-2]
                if rel[-2:] == '_t' and type(val) is str:
                    tail = val
                    rel_t = rel[:-2]
                    if filter_literals and "http://" in tail:
                        dropped_statements += 1
                        raise Exception

            assert head and tail and rel_h and rel_t, f"Weird data point. Some essentials not found. Abort\nD:{datum}"
            assert rel_h == rel_t, f"Weird data point. Head and Tail rels are different. Quitting\nD: {datum}"

            # Drop this bs
            datum.pop(rel_h + '_h')
            datum.pop(rel_t + '_t')
            datum.pop('N')
            conv_datum += [head, rel_h, tail, None, None]

            if len(datum.items()) == 0:
                conv_data.append(tuple(conv_datum))
            else:
                # Get all qualifiers
                for k, v in datum.items():
                    conv_datum[3] = k
                    for _v in v:
                        if filter_literals and "http://" in _v:
                            dropped_quals += 1
                            continue
                        conv_datum[4] = _v
                        conv_data.append(tuple(conv_datum))

        except Exception:
            continue
    print(
        f"\n Dropped {dropped_statements} statements and {dropped_quals} quals with literals \n ")
    return conv_data


def _conv_jf17k_to_quints(data):
    result = []
    for statement in data:
        ents = statement[0::2]
        rels = statement[1::2]

        if len(rels) == 1:
            result.append(statement)
        else:
            s, p, o = statement[0], statement[1], statement[2]
            qual_rel = rels[1:]
            qual_ent = ents[2:]
            for i in range(len(qual_rel)):
                result.append([s, p, o, qual_rel[i], qual_ent[i]])

    return result


def _get_uniques_(train_data: List[Union[tuple, list]],
                  valid_data: List[Union[tuple, list]],
                  test_data: List[Union[tuple, list]]) -> (list, list):
    """ Throw in parsed/wd50k/ files and we'll count the entities and predicates"""

    statement_entities, statement_predicates = [], []

    for statement in train_data + valid_data + test_data:
        statement_entities += statement[::2]
        statement_predicates += statement[1::2]

    statement_entities = sorted(list(set(statement_entities)))
    statement_predicates = sorted(list(set(statement_predicates)))

    return statement_entities, statement_predicates


def _pad_statements_(data: List[list], maxlen: int) -> List[list]:
    """ Padding index is always 0 as in the embedding layers of models. Cool? Cool. """
    result = [
        statement + [0] * (maxlen - len(statement)) if len(statement) < maxlen else statement[
                                                                                    :maxlen] for
        statement in data]
    return result


def clean_literals(data: List[list]) -> List[list]:
    """

    :param data: triples [s, p, o] with possible literals
    :return: triples [s,p,o] without literals

    """
    result = []
    for triple in data:
        if "http://" not in triple[2]:
            result.append(triple)

    return result


def remove_dups(data: List[list]) -> List[list]:
    """

    :param data: list of lists with possible duplicates
    :return: a list without duplicates
    """
    new_l = []
    for datum in tqdm(data):
        if datum not in new_l:
            new_l.append(datum)

    return new_l


def load_wd50k_quints() -> Dict:
    """

    :return:
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k'
    with open(wd50k_DIR / 'train_quints.pkl', 'rb') as f:
        train_quints = pickle.load(f)
    with open(wd50k_DIR / 'valid_quints.pkl', 'rb') as f:
        valid_quints = pickle.load(f)
    with open(wd50k_DIR / 'test_quints.pkl', 'rb') as f:
        test_quints = pickle.load(f)

    quints_entities, quints_predicates = [], []

    for quint in train_quints + valid_quints + test_quints:
        quints_entities += [quint[0], quint[2]]
        if quint[4]:
            quints_entities.append(quint[4])

        quints_predicates.append(quint[1])
        if quint[3]:
            quints_predicates.append(quint[3])

    quints_entities = sorted(list(set(quints_entities)))
    quints_predicates = sorted(list(set(quints_predicates)))

    q_entities = ['__na__'] + quints_entities
    q_predicates = ['__na__'] + quints_predicates

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(q_entities)}
    prtoid = {pred: i for i, pred in enumerate(q_predicates)}

    train = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in train_quints]
    valid = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in valid_quints]
    test = [[entoid[q[0]],
             prtoid[q[1]],
             entoid[q[2]],
             prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
             entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in test_quints]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(q_entities),
            "n_relations": len(q_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_triples() -> Dict:
    """

    :return:
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k'

    with open(wd50k_DIR / 'train_triples.pkl', 'rb') as f:
        train_triples = pickle.load(f)
    with open(wd50k_DIR / 'valid_triples.pkl', 'rb') as f:
        valid_triples = pickle.load(f)
    with open(wd50k_DIR / 'test_triples.pkl', 'rb') as f:
        test_triples = pickle.load(f)

    triples_entities, triples_predicates = [], []

    for triple in train_triples + valid_triples + test_triples:
        triples_entities += [triple[0], triple[2]]
        triples_predicates.append(triple[1])

    triples_entities = ['__na__'] + sorted(list(set(triples_entities)))
    triples_predicates = ['__na__'] + sorted(list(set(triples_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(triples_entities)}
    prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

    train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in train_triples]
    valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in valid_triples]
    test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_triples]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(triples_entities),
            "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_statements(maxlen: int) -> Dict:
    """
        Pull up data from parsed data (thanks magic mike!) and preprocess it to death.
    :return: dict
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k'
    with open(wd50k_DIR / 'train_statements.pkl', 'rb') as f:
        train_statements = pickle.load(f)
    with open(wd50k_DIR / 'valid_statements.pkl', 'rb') as f:
        valid_statements = pickle.load(f)
    with open(wd50k_DIR / 'test_statements.pkl', 'rb') as f:
        test_statements = pickle.load(f)

    statement_entities, statement_predicates = _get_uniques_(train_data=train_statements,
                                                             valid_data=valid_statements,
                                                             test_data=test_statements)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in train_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in valid_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in test_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    train, valid, test = _pad_statements_(train, maxlen), _pad_statements_(valid,
                                                                           maxlen), _pad_statements_(
        test,
        maxlen)

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_33_quints() -> Dict:
    """

    :return:
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_33'
    with open(wd50k_DIR / 'train_quints.pkl', 'rb') as f:
        train_quints = pickle.load(f)
    with open(wd50k_DIR / 'valid_quints.pkl', 'rb') as f:
        valid_quints = pickle.load(f)
    with open(wd50k_DIR / 'test_quints.pkl', 'rb') as f:
        test_quints = pickle.load(f)

    quints_entities, quints_predicates = [], []

    for quint in train_quints + valid_quints + test_quints:
        quints_entities += [quint[0], quint[2]]
        if quint[4]:
            quints_entities.append(quint[4])

        quints_predicates.append(quint[1])
        if quint[3]:
            quints_predicates.append(quint[3])

    quints_entities = sorted(list(set(quints_entities)))
    quints_predicates = sorted(list(set(quints_predicates)))

    q_entities = ['__na__'] + quints_entities
    q_predicates = ['__na__'] + quints_predicates

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(q_entities)}
    prtoid = {pred: i for i, pred in enumerate(q_predicates)}

    train = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in train_quints]
    valid = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in valid_quints]
    test = [[entoid[q[0]],
             prtoid[q[1]],
             entoid[q[2]],
             prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
             entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in test_quints]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(q_entities),
            "n_relations": len(q_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_33_triples() -> Dict:
    """

    :return:
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_33'

    with open(wd50k_DIR / 'train_triples.pkl', 'rb') as f:
        train_triples = pickle.load(f)
    with open(wd50k_DIR / 'valid_triples.pkl', 'rb') as f:
        valid_triples = pickle.load(f)
    with open(wd50k_DIR / 'test_triples.pkl', 'rb') as f:
        test_triples = pickle.load(f)

    triples_entities, triples_predicates = [], []

    for triple in train_triples + valid_triples + test_triples:
        triples_entities += [triple[0], triple[2]]
        triples_predicates.append(triple[1])

    triples_entities = ['__na__'] + sorted(list(set(triples_entities)))
    triples_predicates = ['__na__'] + sorted(list(set(triples_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(triples_entities)}
    prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

    train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in train_triples]
    valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in valid_triples]
    test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_triples]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(triples_entities),
            "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_33_statements(maxlen: int) -> Dict:
    """
        Pull up data from parsed data (thanks magic mike!) and preprocess it to death.
    :return: dict
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_33'
    with open(wd50k_DIR / 'train_statements.pkl', 'rb') as f:
        train_statements = pickle.load(f)
    with open(wd50k_DIR / 'valid_statements.pkl', 'rb') as f:
        valid_statements = pickle.load(f)
    with open(wd50k_DIR / 'test_statements.pkl', 'rb') as f:
        test_statements = pickle.load(f)

    statement_entities, statement_predicates = _get_uniques_(train_data=train_statements,
                                                             valid_data=valid_statements,
                                                             test_data=test_statements)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in train_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in valid_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in test_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    train, valid, test = _pad_statements_(train, maxlen), _pad_statements_(valid,
                                                                           maxlen), _pad_statements_(
        test,
        maxlen)

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_66_quints() -> Dict:
    """

    :return:
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_66'
    with open(wd50k_DIR / 'train_quints.pkl', 'rb') as f:
        train_quints = pickle.load(f)
    with open(wd50k_DIR / 'valid_quints.pkl', 'rb') as f:
        valid_quints = pickle.load(f)
    with open(wd50k_DIR / 'test_quints.pkl', 'rb') as f:
        test_quints = pickle.load(f)

    quints_entities, quints_predicates = [], []

    for quint in train_quints + valid_quints + test_quints:
        quints_entities += [quint[0], quint[2]]
        if quint[4]:
            quints_entities.append(quint[4])

        quints_predicates.append(quint[1])
        if quint[3]:
            quints_predicates.append(quint[3])

    quints_entities = sorted(list(set(quints_entities)))
    quints_predicates = sorted(list(set(quints_predicates)))

    q_entities = ['__na__'] + quints_entities
    q_predicates = ['__na__'] + quints_predicates

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(q_entities)}
    prtoid = {pred: i for i, pred in enumerate(q_predicates)}

    train = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in train_quints]
    valid = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in valid_quints]
    test = [[entoid[q[0]],
             prtoid[q[1]],
             entoid[q[2]],
             prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
             entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in test_quints]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(q_entities),
            "n_relations": len(q_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_66_triples() -> Dict:
    """

    :return:
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_66'

    with open(wd50k_DIR / 'train_triples.pkl', 'rb') as f:
        train_triples = pickle.load(f)
    with open(wd50k_DIR / 'valid_triples.pkl', 'rb') as f:
        valid_triples = pickle.load(f)
    with open(wd50k_DIR / 'test_triples.pkl', 'rb') as f:
        test_triples = pickle.load(f)

    triples_entities, triples_predicates = [], []

    for triple in train_triples + valid_triples + test_triples:
        triples_entities += [triple[0], triple[2]]
        triples_predicates.append(triple[1])

    triples_entities = ['__na__'] + sorted(list(set(triples_entities)))
    triples_predicates = ['__na__'] + sorted(list(set(triples_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(triples_entities)}
    prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

    train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in train_triples]
    valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in valid_triples]
    test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_triples]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(triples_entities),
            "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_66_statements(maxlen: int) -> Dict:
    """
        Pull up data from parsed data (thanks magic mike!) and preprocess it to death.
    :return: dict
    """

    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_66'
    with open(wd50k_DIR / 'train_statements.pkl', 'rb') as f:
        train_statements = pickle.load(f)
    with open(wd50k_DIR / 'valid_statements.pkl', 'rb') as f:
        valid_statements = pickle.load(f)
    with open(wd50k_DIR / 'test_statements.pkl', 'rb') as f:
        test_statements = pickle.load(f)

    statement_entities, statement_predicates = _get_uniques_(train_data=train_statements,
                                                             valid_data=valid_statements,
                                                             test_data=test_statements)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in train_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in valid_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in test_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    train, valid, test = _pad_statements_(train, maxlen), _pad_statements_(valid,
                                                                           maxlen), _pad_statements_(
        test,
        maxlen)

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_statements(maxlen: int) -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100'
    with open(wd50k_DIR / 'train_statements.pkl', 'rb') as f:
        train_statements = pickle.load(f)
    with open(wd50k_DIR / 'valid_statements.pkl', 'rb') as f:
        valid_statements = pickle.load(f)
    with open(wd50k_DIR / 'test_statements.pkl', 'rb') as f:
        test_statements = pickle.load(f)

    statement_entities, statement_predicates = _get_uniques_(train_data=train_statements,
                                                             valid_data=valid_statements,
                                                             test_data=test_statements)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in train_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in valid_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in test_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    train, valid, test = _pad_statements_(train, maxlen), _pad_statements_(valid,
                                                                           maxlen), _pad_statements_(
        test,
        maxlen)

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_quints() -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100'
    with open(wd50k_DIR / 'train_quints.pkl', 'rb') as f:
        train_quints = pickle.load(f)
    with open(wd50k_DIR / 'valid_quints.pkl', 'rb') as f:
        valid_quints = pickle.load(f)
    with open(wd50k_DIR / 'test_quints.pkl', 'rb') as f:
        test_quints = pickle.load(f)

    quints_entities, quints_predicates = [], []

    for quint in train_quints + valid_quints + test_quints:
        quints_entities += [quint[0], quint[2]]
        if quint[4]:
            quints_entities.append(quint[4])

        quints_predicates.append(quint[1])
        if quint[3]:
            quints_predicates.append(quint[3])

    quints_entities = ['__na__'] + sorted(list(set(quints_entities)))
    quints_predicates = ['__na__'] + sorted(list(set(quints_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(quints_entities)}
    prtoid = {pred: i for i, pred in enumerate(quints_predicates)}

    train = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]],
              entoid[q[4]]] for q in train_quints]
    valid = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]],
              entoid[q[4]]] for q in valid_quints]
    test = [[entoid[q[0]],
             prtoid[q[1]],
             entoid[q[2]],
             prtoid[q[3]],
             entoid[q[4]]] for q in test_quints]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(quints_entities),
            "n_relations": len(quints_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_triples() -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100'

    with open(wd50k_DIR / 'train_triples.pkl', 'rb') as f:
        train_triples = pickle.load(f)
    with open(wd50k_DIR / 'valid_triples.pkl', 'rb') as f:
        valid_triples = pickle.load(f)
    with open(wd50k_DIR / 'test_triples.pkl', 'rb') as f:
        test_triples = pickle.load(f)

    triples_entities, triples_predicates = [], []

    for triple in train_triples + valid_triples + test_triples:
        triples_entities += [triple[0], triple[2]]
        triples_predicates.append(triple[1])

    triples_entities = ['__na__'] + sorted(list(set(triples_entities)))
    triples_predicates = ['__na__'] + sorted(list(set(triples_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(triples_entities)}
    prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

    train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in train_triples]
    valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in valid_triples]
    test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_triples]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(triples_entities),
            "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_33_statements(maxlen: int) -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100_33'
    with open(wd50k_DIR / 'train_statements.pkl', 'rb') as f:
        train_statements = pickle.load(f)
    with open(wd50k_DIR / 'valid_statements.pkl', 'rb') as f:
        valid_statements = pickle.load(f)
    with open(wd50k_DIR / 'test_statements.pkl', 'rb') as f:
        test_statements = pickle.load(f)

    statement_entities, statement_predicates = _get_uniques_(train_data=train_statements,
                                                             valid_data=valid_statements,
                                                             test_data=test_statements)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in train_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in valid_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in test_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    train, valid, test = _pad_statements_(train, maxlen), _pad_statements_(valid,
                                                                           maxlen), _pad_statements_(
        test,
        maxlen)

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_33_quints() -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100_33'
    with open(wd50k_DIR / 'train_quints.pkl', 'rb') as f:
        train_quints = pickle.load(f)
    with open(wd50k_DIR / 'valid_quints.pkl', 'rb') as f:
        valid_quints = pickle.load(f)
    with open(wd50k_DIR / 'test_quints.pkl', 'rb') as f:
        test_quints = pickle.load(f)

    quints_entities, quints_predicates = [], []

    for quint in train_quints + valid_quints + test_quints:
        quints_entities += [quint[0], quint[2]]
        if quint[4]:
            quints_entities.append(quint[4])

        quints_predicates.append(quint[1])
        if quint[3]:
            quints_predicates.append(quint[3])

    quints_entities = ['__na__'] + sorted(list(set(quints_entities)))
    quints_predicates = ['__na__'] + sorted(list(set(quints_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(quints_entities)}
    prtoid = {pred: i for i, pred in enumerate(quints_predicates)}

    train = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in train_quints]
    valid = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in valid_quints]
    test = [[entoid[q[0]],
             prtoid[q[1]],
             entoid[q[2]],
             prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
             entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in test_quints]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(quints_entities),
            "n_relations": len(quints_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_33_triples() -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100_33'

    with open(wd50k_DIR / 'train_triples.pkl', 'rb') as f:
        train_triples = pickle.load(f)
    with open(wd50k_DIR / 'valid_triples.pkl', 'rb') as f:
        valid_triples = pickle.load(f)
    with open(wd50k_DIR / 'test_triples.pkl', 'rb') as f:
        test_triples = pickle.load(f)

    triples_entities, triples_predicates = [], []

    for triple in train_triples + valid_triples + test_triples:
        triples_entities += [triple[0], triple[2]]
        triples_predicates.append(triple[1])

    triples_entities = ['__na__'] + sorted(list(set(triples_entities)))
    triples_predicates = ['__na__'] + sorted(list(set(triples_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(triples_entities)}
    prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

    train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in train_triples]
    valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in valid_triples]
    test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_triples]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(triples_entities),
            "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_66_statements(maxlen: int) -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100_66'
    with open(wd50k_DIR / 'train_statements.pkl', 'rb') as f:
        train_statements = pickle.load(f)
    with open(wd50k_DIR / 'valid_statements.pkl', 'rb') as f:
        valid_statements = pickle.load(f)
    with open(wd50k_DIR / 'test_statements.pkl', 'rb') as f:
        test_statements = pickle.load(f)

    statement_entities, statement_predicates = _get_uniques_(train_data=train_statements,
                                                             valid_data=valid_statements,
                                                             test_data=test_statements)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in train_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in valid_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in test_statements:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    train, valid, test = _pad_statements_(train, maxlen), _pad_statements_(valid, maxlen), \
                         _pad_statements_(test, maxlen)

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_66_quints() -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100_66'
    with open(wd50k_DIR / 'train_quints.pkl', 'rb') as f:
        train_quints = pickle.load(f)
    with open(wd50k_DIR / 'valid_quints.pkl', 'rb') as f:
        valid_quints = pickle.load(f)
    with open(wd50k_DIR / 'test_quints.pkl', 'rb') as f:
        test_quints = pickle.load(f)

    quints_entities, quints_predicates = [], []

    for quint in train_quints + valid_quints + test_quints:
        quints_entities += [quint[0], quint[2]]
        if quint[4]:
            quints_entities.append(quint[4])

        quints_predicates.append(quint[1])
        if quint[3]:
            quints_predicates.append(quint[3])

    quints_entities = ['__na__'] + sorted(list(set(quints_entities)))
    quints_predicates = ['__na__'] + sorted(list(set(quints_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(quints_entities)}
    prtoid = {pred: i for i, pred in enumerate(quints_predicates)}

    train = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__nPARSED_DATA_DIRa__']] for q in train_quints]
    valid = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in valid_quints]
    test = [[entoid[q[0]],
             prtoid[q[1]],
             entoid[q[2]],
             prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
             entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in test_quints]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(quints_entities),
            "n_relations": len(quints_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wd50k_100_66_triples() -> Dict:
    # Load data from disk
    wd50k_DIR = LOC.parsed / 'wd50k_100_66'

    with open(wd50k_DIR / 'train_triples.pkl', 'rb') as f:
        train_triples = pickle.load(f)
    with open(wd50k_DIR / 'valid_triples.pkl', 'rb') as f:
        valid_triples = pickle.load(f)
    with open(wd50k_DIR / 'test_triples.pkl', 'rb') as f:
        test_triples = pickle.load(f)

    triples_entities, triples_predicates = [], []

    for triple in train_triples + valid_triples + test_triples:
        triples_entities += [triple[0], triple[2]]
        triples_predicates.append(triple[1])

    triples_entities = ['__na__'] + sorted(list(set(triples_entities)))
    triples_predicates = ['__na__'] + sorted(list(set(triples_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(triples_entities)}
    prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

    train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in train_triples]
    valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in valid_triples]
    test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_triples]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(triples_entities),
            "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wikipeople_quints(filter_literals=True):
    # Load data from disk
    DIRNAME = Path('./data/raw/wikipeople')

    # Load raw shit
    with open(DIRNAME / 'n-ary_train.json', 'r') as f:
        raw_trn = []
        for line in f.readlines():
            raw_trn.append(json.loads(line))

    with open(DIRNAME / 'n-ary_test.json', 'r') as f:
        raw_tst = []
        for line in f.readlines():
            raw_tst.append(json.loads(line))

    with open(DIRNAME / 'n-ary_valid.json', 'r') as f:
        raw_val = []
        for line in f.readlines():
            raw_val.append(json.loads(line))

    # raw_trn[:-10], raw_tst[:10], raw_val[:10]
    # Conv data to our format
    conv_trn, conv_tst, conv_val = _conv_to_our_quint_format_(raw_trn,
                                                              filter_literals=filter_literals), \
                                   _conv_to_our_quint_format_(raw_tst,
                                                              filter_literals=filter_literals), \
                                   _conv_to_our_quint_format_(raw_val,
                                                              filter_literals=filter_literals)

    # quints_entities, quints_predicates = _get_uniques_(train_data=conv_trn,
    #                                                          test_data=conv_tst,
    #                                                          valid_data=conv_val)

    # st_entities = ['__na__'] + quints_entities
    # st_predicates = ['__na__'] + quints_predicates
    # quints_entities = ['__na__'] + sorted(list(set(quints_entities)))
    # quints_predicates = ['__na__'] + sorted(list(set(quints_predicates)))
    quints_entities, quints_predicates = [], []
    for quint in conv_trn + conv_val + conv_tst:
        quints_entities += [quint[0], quint[2]]
        if quint[4]:
            quints_entities.append(quint[4])

        quints_predicates.append(quint[1])
        if quint[3]:
            quints_predicates.append(quint[3])

    quints_entities = sorted(list(set(quints_entities)))
    quints_predicates = sorted(list(set(quints_predicates)))

    q_entities = ['__na__'] + quints_entities
    q_predicates = ['__na__'] + quints_predicates

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(q_entities)}
    prtoid = {pred: i for i, pred in enumerate(q_predicates)}

    train = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in conv_trn]
    valid = [[entoid[q[0]],
              prtoid[q[1]],
              entoid[q[2]],
              prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
              entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in conv_val]
    test = [[entoid[q[0]],
             prtoid[q[1]],
             entoid[q[2]],
             prtoid[q[3]] if q[3] is not None else prtoid['__na__'],
             entoid[q[4]] if q[4] is not None else entoid['__na__']] for q in conv_tst]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(q_entities),
            "n_relations": len(q_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wikipeople_triples(filter_literals=True):
    # Load data from disk
    WP_DIR = LOC.parsed / 'wikipeople'

    with open(WP_DIR / 'train_triples.pkl', 'rb') as f:
        train_triples = pickle.load(f)
    with open(WP_DIR / 'valid_triples.pkl', 'rb') as f:
        valid_triples = pickle.load(f)
    with open(WP_DIR / 'test_triples.pkl', 'rb') as f:
        test_triples = pickle.load(f)

    triples_entities, triples_predicates = [], []

    if filter_literals:
        train_triples = clean_literals(train_triples)
        valid_triples = clean_literals(valid_triples)
        test_triples = clean_literals(test_triples)

    for triple in train_triples + valid_triples + test_triples:
        triples_entities += [triple[0], triple[2]]
        triples_predicates.append(triple[1])

    triples_entities = ['__na__'] + sorted(list(set(triples_entities)))
    triples_predicates = ['__na__'] + sorted(list(set(triples_predicates)))

    # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
    entoid = {pred: i for i, pred in enumerate(triples_entities)}
    prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

    train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in train_triples]
    valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in valid_triples]
    test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_triples]

    return {"train": train, "valid": valid, "test": test, "n_entities": len(triples_entities),
            "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wikipeople_statements(maxlen=17, filter_literals=True) -> Dict:
    """
        :return: train/valid/test splits for the wikipeople dataset in its quints form
    """
    DIRNAME = Path('./data/raw/wikipeople')

    with open(DIRNAME / 'n-ary_train.json', 'r') as f:
        raw_trn = []
        for line in f.readlines():
            raw_trn.append(json.loads(line))

    with open(DIRNAME / 'n-ary_test.json', 'r') as f:
        raw_tst = []
        for line in f.readlines():
            raw_tst.append(json.loads(line))

    with open(DIRNAME / 'n-ary_valid.json', 'r') as f:
        raw_val = []
        for line in f.readlines():
            raw_val.append(json.loads(line))

    # raw_trn[:-10], raw_tst[:10], raw_val[:10]
    # Conv data to our format
    conv_trn, conv_tst, conv_val = _conv_to_our_format_(raw_trn, filter_literals=filter_literals), \
                                   _conv_to_our_format_(raw_tst, filter_literals=filter_literals), \
                                   _conv_to_our_format_(raw_val, filter_literals=filter_literals)
    # conv_trn, conv_tst, conv_val = remove_dups(conv_trn), remove_dups(conv_tst), remove_dups(conv_val)
    # Get uniques
    statement_entities, statement_predicates = _get_uniques_(train_data=conv_trn,
                                                             test_data=conv_tst,
                                                             valid_data=conv_val)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in conv_trn:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in conv_val:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in conv_tst:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    train, valid, test = _pad_statements_(train, maxlen), _pad_statements_(valid,
                                                                           maxlen), _pad_statements_(
        test,
        maxlen)

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_jf17k_triples() -> Dict:
    PARSED_DIR = Path('./data/parsed/jf17k')

    training_statements = []
    test_statements = []

    with open(PARSED_DIR / 'train.txt', 'r') as train_file, \
            open(PARSED_DIR / 'test.txt', 'r') as test_file:

        for line in train_file:
            training_statements.append(line.strip("\n").split(","))

        for line in test_file:
            test_statements.append(line.strip("\n").split(","))

        entities, predicates = [], []
        for s in training_statements + test_statements:
            entities += [s[0], s[2]]
            predicates.append(s[1])

        triples_entities = ['__na__'] + sorted(list(set(entities)))
        triples_predicates = ['__na__'] + sorted(list(set(predicates)))

        # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
        entoid = {pred: i for i, pred in enumerate(triples_entities)}
        prtoid = {pred: i for i, pred in enumerate(triples_predicates)}

        # sample valid as 20% of train
        random.shuffle(training_statements)
        tr_st = training_statements[:int(0.8 * len(training_statements))]
        val_st = training_statements[int(0.8 * len(training_statements)):]

        train = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in tr_st]
        valid = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in val_st]
        test = [[entoid[q[0]], prtoid[q[1]], entoid[q[2]]] for q in test_statements]

        # optional
        clean_train, clean_valid, clean_test = remove_dups(train), remove_dups(valid), remove_dups(
            test)

        return {"train": clean_train, "valid": clean_valid, "test": clean_test,
                "n_entities": len(triples_entities),
                "n_relations": len(triples_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_jf17k_quints() -> Dict:
    PARSED_DIR = Path('./data/parsed/jf17k')

    training_statements = []
    test_statements = []

    with open(PARSED_DIR / 'train.txt', 'r') as train_file, \
            open(PARSED_DIR / 'test.txt', 'r') as test_file:

        for line in train_file:
            training_statements.append(line.strip("\n").split(","))

        for line in test_file:
            test_statements.append(line.strip("\n").split(","))

        # sample valid as 20% of train
        random.shuffle(training_statements)
        tr_st = training_statements[:int(0.8 * len(training_statements))]
        val_st = training_statements[int(0.8 * len(training_statements)):]

        train_quints = _conv_jf17k_to_quints(tr_st)
        val_quints = _conv_jf17k_to_quints(val_st)
        test_quints = _conv_jf17k_to_quints(test_statements)

        quints_entities, quints_predicates = [], []
        for quint in train_quints + val_quints + test_quints:
            quints_entities += [quint[0], quint[2]]
            quints_predicates.append(quint[1])
            if len(quint) > 3:
                quints_entities.append(quint[4])
                quints_predicates.append(quint[3])

        quints_entities = sorted(list(set(quints_entities)))
        quints_predicates = sorted(list(set(quints_predicates)))

        q_entities = ['__na__'] + quints_entities
        q_predicates = ['__na__'] + quints_predicates

        # uritoid = {ent: i for i, ent in enumerate(['__na__', '__pad__'] + entities +  predicates)}
        entoid = {pred: i for i, pred in enumerate(q_entities)}
        prtoid = {pred: i for i, pred in enumerate(q_predicates)}

        train = [[entoid[q[0]],
                  prtoid[q[1]],
                  entoid[q[2]],
                  prtoid[q[3]] if len(q) > 3 else prtoid['__na__'],
                  entoid[q[4]] if len(q) > 3 else entoid['__na__']] for q in train_quints]
        valid = [[entoid[q[0]],
                  prtoid[q[1]],
                  entoid[q[2]],
                  prtoid[q[3]] if len(q) > 3 else prtoid['__na__'],
                  entoid[q[4]] if len(q) > 3 else entoid['__na__']] for q in val_quints]
        test = [[entoid[q[0]],
                 prtoid[q[1]],
                 entoid[q[2]],
                 prtoid[q[3]] if len(q) > 3 else prtoid['__na__'],
                 entoid[q[4]] if len(q) > 3 else entoid['__na__']] for q in test_quints]

        return {"train": train, "valid": valid, "test": test, "n_entities": len(q_entities),
                "n_relations": len(q_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_jf17k_statements(maxlen=15) -> Dict:
    PARSED_DIR = Path('./data/parsed/jf17k')

    training_statements = []
    test_statements = []

    with open(PARSED_DIR / 'train.txt', 'r') as train_file, \
            open(PARSED_DIR / 'test.txt', 'r') as test_file:

        for line in train_file:
            training_statements.append(line.strip("\n").split(","))

        for line in test_file:
            test_statements.append(line.strip("\n").split(","))

        st_entities, st_predicates = _get_uniques_(training_statements, test_statements,
                                                   test_statements)
        st_entities = ['__na__'] + st_entities
        st_predicates = ['__na__'] + st_predicates

        entoid = {pred: i for i, pred in enumerate(st_entities)}
        prtoid = {pred: i for i, pred in enumerate(st_predicates)}

        # sample valid as 20% of train
        random.shuffle(training_statements)
        tr_st = training_statements[:int(0.8 * len(training_statements))]
        val_st = training_statements[int(0.8 * len(training_statements)):]

        train, valid, test = [], [], []
        for st in tr_st:
            id_st = []
            for i, uri in enumerate(st):
                id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
            train.append(id_st)

        for st in val_st:
            id_st = []
            for i, uri in enumerate(st):
                id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
            valid.append(id_st)

        for st in test_statements:
            id_st = []
            for i, uri in enumerate(st):
                id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
            test.append(id_st)

        train, valid, test = _pad_statements_(train, maxlen), \
                             _pad_statements_(valid, maxlen), \
                             _pad_statements_(test, maxlen)

        return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
                "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def load_wn(name: str, subtype: str, train_on_test: bool = False, maxlen: int = 43) -> Dict:
    """
        :return: train/valid/test splits for the wd50k datasets
    """
    assert name in ['wn_synset', 'wn_synset_mined', 'wn_synset_unirel'], \
        "Incorrect dataset"  # TODO: add to this list as more things are implemented
    assert subtype in ["triples", "quints", "statements"], \
        "Incorrect subtype: triples/quints/statements"

    if train_on_test:
        raw_dirname = LOC.parsed / f'{name}/{subtype}' / 'train_on_test'
        proc_dirname = LOC.cleaned / f'{name}/{subtype}' / 'train_on_test'
    else:
        raw_dirname = LOC.parsed / f'{name}/{subtype}'
        proc_dirname = LOC.cleaned / f'{name}/{subtype}'

    cleaned_data = _safe_load_from_disk_(proc_dirname, maxlen=maxlen)
    if cleaned_data is not False:
        return {**cleaned_data}

    # Load raw stuff
    with open(raw_dirname / 'train.txt', 'r') as f:
        raw_trn = []
        for line in f.readlines():
            raw_trn.append(line.strip("\n").split(","))

    with open(raw_dirname / 'test.txt', 'r') as f:
        raw_tst = []
        for line in f.readlines():
            raw_tst.append(line.strip("\n").split(","))

    with open(raw_dirname / 'valid.txt', 'r') as f:
        raw_val = []
        for line in f.readlines():
            raw_val.append(line.strip("\n").split(","))

    # Get uniques
    statement_entities, statement_predicates = _get_uniques_(train_data=raw_trn,
                                                             test_data=raw_tst,
                                                             valid_data=raw_val)

    st_entities = ['__na__'] + statement_entities
    st_predicates = ['__na__'] + statement_predicates

    entoid = {pred: i for i, pred in enumerate(st_entities)}
    prtoid = {pred: i for i, pred in enumerate(st_predicates)}

    train, valid, test = [], [], []
    for st in raw_trn:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        train.append(id_st)
    for st in raw_val:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        valid.append(id_st)
    for st in raw_tst:
        id_st = []
        for i, uri in enumerate(st):
            id_st.append(entoid[uri] if i % 2 == 0 else prtoid[uri])
        test.append(id_st)

    if subtype != "triples":
        if subtype == "quints":
            maxlen = 5
        train, valid, test = _pad_statements_(train, maxlen), \
                             _pad_statements_(valid, maxlen), \
                             _pad_statements_(test, maxlen)

    if subtype == "triples" or subtype == "quints":
        train, valid, test = remove_dups(train), remove_dups(valid), remove_dups(test)

    _safe_dump_to_disk_(proc_dirname, train=train, valid=valid, test=test, entoid=entoid,
                        prtoid=prtoid, config={'n_entities': len(st_entities),
                                               'n_relations': len(st_predicates),
                                               'maxlen': maxlen})

    return {"train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid}


def count_stats(dataset):
    import collections
    tr = dataset['train']
    vl = dataset['valid']
    ts = dataset['test']
    ne = dataset['n_entities']
    nr = dataset['n_relations']
    print("Magic Mike!")
    print(
        f"The dataset: {len(tr)} training, {len(vl)} val, {len(ts)} test, {ne} entities, {nr} rels")
    if len(tr[0]) > 3:
        quals_train = len([x for x in tr if x[3] != 0])
        quals_val = len([x for x in vl if x[3] != 0])
        quals_test = len([x for x in ts if x[3] != 0])
        max_qlen = max([np.array(x).nonzero()[0][-1] + 1 for x in tr + vl])
        distr = {k: v for k, v in
                 collections.Counter(np.array(x).nonzero()[0][-1] + 1 for x in tr + vl).items()}
        print(f"W/ quals - train: {quals_train}/{round(float(quals_train / len(tr)), 3)}")
        print(f"W/ quals - val: {quals_val}/{round(float(quals_val / len(vl)), 3)}")
        print(f"W/ quals - test: {quals_test}/{round(float(quals_test / len(ts)), 3)}")
        print(f"Max statement len w/ qual: {max_qlen}")
        print(distr)
    # id2e = {v:k for k,v in dataset['e2id'].items()}
    # id2p = {v:k for k,v in dataset['r2id'].items()}
    train_ents = set([item for x in tr for item in x[0::2]])
    train_rels = set([item for x in tr for item in x[1::2]])
    val_ents = set([item for x in vl for item in x[0::2]])
    val_rels = set([item for x in vl for item in x[1::2]])
    tv_ents = set([item for x in tr + vl for item in x[0::2]])
    tv_rels = set([item for x in tr + vl for item in x[1::2]])
    test_ents = set([item for x in ts for item in x[0::2]])
    test_rels = set([item for x in ts for item in x[1::2]])
    if len(tr[0]) > 3:
        qe = set([item for x in tr + vl + ts for item in x[4::2]])
        qr = set([item for x in tr + vl + ts for item in x[3::2]])
        all_ents = set([item for x in tr + vl + ts for item in [x[0], x[2]]])
        all_rels = set([x[1] for x in tr + vl + ts])
        print(f"Unique qual entities: {len(qe.difference(all_ents))}")
        print(f"Unique qual rels: {len(qr.difference(all_rels))}")

    dups_train = {k: v for k, v in collections.Counter(tuple(x) for x in tr).items() if v > 1}
    print("Duplicates in train: ", len(dups_train))
    dups_val = {k: v for k, v in collections.Counter(tuple(x) for x in vl).items() if v > 1}
    print("Duplicates in val: ", len(dups_val))
    dups_test = {k: v for k, v in collections.Counter(tuple(x) for x in ts).items() if v > 1}
    print("Duplicates in test: ", len(dups_test))
    print("-" * 10)

    ts_unique = val_ents.difference(train_ents)
    ts_unique_rel = val_rels.difference(train_rels)
    senseless_triples = []
    for x in vl:
        xe = set(x[0::2])
        xr = set(x[1::2])
        if len(xe.intersection(ts_unique)) > 0:
            senseless_triples.append(x)
            continue
        elif len(xr.intersection(ts_unique_rel)) > 0:
            senseless_triples.append(x)
            continue

    count = 0
    val_spos = set([(i[0], i[1], i[2]) for i in vl])
    for i in tr:
        main_triple = (i[0], i[1], i[2])
        if main_triple in val_spos:
            count += 1

    # senseless_triples = [x for x in vl if x[0] in ts_unique or x[2] in ts_unique]
    print(len(ts_unique), "/", ne, " entities are in val but not in train")
    print(len(ts_unique_rel), "/", nr, " rels are in val but not in train")
    print(f"Those entities and relations are used in {len(senseless_triples)} val triples")
    print("Leak triples in train wrt val: ", count, "/", len(tr))

    print("-" * 10)
    ts_unique = test_ents.difference(tv_ents)
    ts_unique_rel = test_rels.difference(tv_rels)
    senseless_triples = []
    for x in ts:
        xe = set(x[0::2])
        xr = set(x[1::2])
        if len(xe.intersection(ts_unique)) > 0:
            senseless_triples.append(x)
            continue
        elif len(xr.intersection(ts_unique_rel)) > 0:
            senseless_triples.append(x)
            continue

    count = 0
    test_spos = set([(i[0], i[1], i[2]) for i in ts])
    for i in tr + vl:
        main_triple = (i[0], i[1], i[2])
        if main_triple in test_spos:
            count += 1
    # senseless_triples = [x for x in ts if x[0] in ts_unique or x[2] in ts_unique]
    print(len(ts_unique), "/", ne, " entities are in test but not in train+valid")
    print(len(ts_unique_rel), "/", nr, " rels are in test but not in train")
    print(f"Those entities and relations are used in {len(senseless_triples)} test triples")
    print("Leak triples in train+val wrt to test : ", count, "/", len(tr + vl))

    count = 0
    test_spos = set([(i[0], i[1], i[2]) for i in ts])
    test_opss = set([(i[2], i[1], i[0]) for i in ts])
    for i in tr + vl:
        main_triple = (i[0], i[1], i[2])
        rec_triple = (i[2], i[1], i[0])
        if main_triple in test_opss:
            count += 1
        if rec_triple in test_spos:
            count += 1
    # senseless_triples = [x for x in ts if x[0] in ts_unique or x[2] in ts_unique]
    print("Reverse triples in train+val wrt to test : ", count, "/", len(tr + vl))

    count = 0
    count_rev = 0
    trvl_spos = set([(i[0], i[1], i[2]) for i in tr + vl])
    for i in ts:
        main_triple = (i[0], i[1], i[2])
        rec_triple = (i[2], i[1], i[0])
        if main_triple in trvl_spos:
            count += 1
        if rec_triple in trvl_spos:
            count_rev += 1

    print("Triples in test that share the same base as in train+val : ", count, "/", len(ts))
    print("Triples in test that  are reverses of those in train+val : ", count_rev, "/", len(ts))


def _safe_load_from_disk_(loc: Path, maxlen: int):
    """
        See if things exist. If so, try loading them. Return all or return None

        Scope: WordNet stuff
        To Return: "train": train, "valid": valid, "test": test, "n_entities": len(st_entities),
            "n_relations": len(st_predicates), 'e2id': entoid, 'r2id': prtoid
    """
    # Check if parsed already exists?
    if not Path(loc / 'train.pkl').exists() or not Path(loc / 'test.pkl').exists() \
            or not Path(loc / 'valid.pkl').exists():
        return False

    # Check if the config lines up
    try:
        # Check if the config was the same
        with Path(loc / 'config.json').open(mode='r', encoding='utf8') as f:
            assert json.load(f)['maxlen'] == maxlen, \
                "Need to reclean the dataset, config did not add up"
    except (FileNotFoundError, json.JSONDecodeError, AssertionError):
        print("Need to reclean the dataset. This will take 5-10 minutes")
        return False

    # Assume we have all the stuff and proceed
    train: List[list] = pickle.load((loc / 'train.pkl').open('rb'))
    test: List[list] = pickle.load((loc / 'test.pkl').open('rb'))
    valid: List[list] = pickle.load((loc / 'valid.pkl').open('rb'))
    with Path(loc / 'config.json').open(mode='r', encoding='utf8') as f:
        datum = json.load(f)
        n_entities = datum['n_entities']
        n_relations = datum['n_relations']
    entoid = pickle.load((loc / 'entoid.pkl').open('rb'))
    prtoid = pickle.load((loc / 'prtoid.pkl').open('rb'))

    return {"train": train, "valid": valid, "test": test, "n_entities": n_entities,
            "n_relations": n_relations, 'e2id': entoid, 'r2id': prtoid}


def _safe_dump_to_disk_(loc: Path, train, valid, test, entoid, prtoid, config):
    """ Pickle dump this stuff to disk """
    # assert loc.is_dir(), "Location is not a directory"
    loc.mkdir(parents=True, exist_ok=True)

    pickle.dump(train, Path(loc / 'train.pkl').open(mode='wb+'))
    pickle.dump(valid, Path(loc / 'valid.pkl').open(mode='wb+'))
    pickle.dump(test, Path(loc / 'test.pkl').open(mode='wb+'))
    pickle.dump(entoid, Path(loc / 'entoid.pkl').open(mode='wb+'))
    pickle.dump(prtoid, Path(loc / 'prtoid.pkl').open(mode='wb+'))
    json.dump(config, Path(loc / 'config.json').open(mode='w+'))

    print(f"Processed dataset successfully cached at {loc}")


if __name__ == "__main__":
    ds = load_wikipeople_statements(maxlen=15)
    count_stats(ds)
