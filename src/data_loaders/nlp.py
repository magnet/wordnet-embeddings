"""
    This script contains data loader functions for the following NLP datasets:

        - SimLex999 (SimLex-999: Evaluating Semantic Models with (Genuine) Similarity Estimation.
                        2014. Felix Hill, Roi Reichart and Anna Korhonen.
                        Computational Linguistics. 2015)

    The format in which these are made available varies
        but generally, each split is an array
        where each data point contains an X and a Y
"""
from typing import Dict
from mytorch.utils.goodies import convert_nicely

# Local Imports
from config import LOCATIONS as LOC


def _load_sl999_() -> Dict[str, dict]:
    """
        Returns a {train: {...}, valid: {...}, test: {'X': [...], 'Y':[...]}}

        Each value is  List[Tuple[str, str]], List[float] where
            the first contains [(wide, narrow), (old, new) ...], and
            the second contains [0.1, 0.5 ...]


    """

    floc = LOC.raw / 'sl999' / 'SimLex-999.txt'
    with floc.open('r', encoding='utf8') as f:
        raw = f.readlines()

    X, Y = [], []
    for line in raw[1:]:  # First line is column labels
        _parsed = [convert_nicely(x) for x in line.replace('\n', '').split('\t')[:4]]

        # Sanity Checks
        assert type(_parsed[0]) is str and type(_parsed[1]) is str, f"Either '{_parsed[0]}" \
                                                                    f"or '{_parsed[1]} is not str"
        assert type(_parsed[3]) is float, f"{_parsed[3]} is not a float"

        X.append((_parsed[:2]))
        Y.append(_parsed[3])

    return {'train': {'X': [], 'Y': []}, 'valid': {'X': [], 'Y': []}, 'test': {'X': X, 'Y': Y}}


if __name__ == '__main__':
    data = _load_sl999_()
    print(data.__len__())