"""
    Authored: Priyansh Trivedi
    Purpose: To parse WordNet from NLTK and manual schema to whatever form is required.
    Output: unlabeled CSV (typically triples or statements)

    Implemented Methods:
        Considering a fixed set of relations between synset nodes represented as a relational graph
"""
import json
from typing import List, Dict

import nltk
import numpy as np

# Local imports
import _pathfix
from config import LOCATIONS as LOC

nltk.data.path.append("/home/priyansh/Dev/perm/nltk/nltk_data")  # TODO: look for a permanent fix
from nltk.corpus import wordnet as wn

# Local Imports
from utils.main import dump_to_disk


def _get_all_relations_(synset: nltk.corpus.reader.wordnet.Synset, valid_relations: List[str]) \
        -> Dict[str, List[nltk.corpus.reader.wordnet.Synset]]:
    """ Extract all relations from a given synset (based on a fixed relation list) """
    data: Dict = {relation: getattr(synset, relation)() for relation in valid_relations}

    # Remove all empty rows
    data = {k: v for k, v in data.items() if v != []}

    return data


def extract_synset_graph(train_on_test: bool = False) -> None:
    """
        1. Pull the wordnet relations from schema file
        2. Iterate over all synsets
            - for each synset, get all relations
            - convert the synsets to names and write to a list
        3. Dump list to the parsed folder
    """

    data: List[List[str]] = []

    # Pull the wordnet relations file
    schema: Dict = json.load(open(LOC.wn_schema))

    print("Starting to iterate over all synsets. It's gonna take a couple seconds.")

    # Iterate over all synsets
    for _subject in wn.all_synsets():

        # Get all relations for this synset
        triples = _get_all_relations_(_subject, schema['relations'])

        # Convert it all to names and push to data (hint: synset is subject)
        for _relation, _objects in triples.items():
            data += [[_subject.name(), _relation, _object.name()] for _object in _objects]

    # Divide the triples are train, valid or test
    dl = len(data)
    np.random.shuffle(data)
    if train_on_test:
        # We have everything in train split. That's because we want embeddings for all
        train, test, valid = data, data[int(dl*0.7): int(dl*0.85)], data[int(dl*0.85):]

        # Dump data to the parsed folder
        dump_to_disk(train, LOC.parsed / 'wn_synset' / 'triples' / 'train_on_test' / 'train.txt')
        dump_to_disk(valid, LOC.parsed / 'wn_synset' / 'triples' / 'train_on_test' / 'valid.txt')
        dump_to_disk(test, LOC.parsed / 'wn_synset' / 'triples' / 'train_on_test' / 'test.txt')
    else:
        train, test, valid = data[: int(dl*0.7)], data[int(dl*0.7): int(dl*0.85)], data[int(dl*0.85):]

        # Dump data to the parsed folder
        dump_to_disk(train, LOC.parsed / 'wn_synset' / 'triples' / 'train.txt')
        dump_to_disk(valid, LOC.parsed / 'wn_synset' / 'triples' / 'valid.txt')
        dump_to_disk(test, LOC.parsed / 'wn_synset' / 'triples' / 'test.txt')


if __name__ == '__main__':
    _pathfix.suppress_unused_import()

    extract_synset_graph(train_on_test=True)
