import os

os.environ['MKL_NUM_THREADS'] = '1'
import sys
import torch
import wandb
import random
import pickle
import numpy as np
from pathlib import Path
from functools import partial
from typing import Dict, Optional, List
from mytorch.utils.goodies import parse_args, BadParameters, tosave

# Local imports
from loops.evaluation import EvaluationBenchGNNMultiClass
from config import ARGS, GCN_ARGS, WN_ARGS, LOCATIONS as LOC
from utils.main import get_saved_wandb_id, combine_splits
from data_loaders.data_manager import KGDataManager
from models.models import StarETransformerTriples
from utils.metrics import acc, mrr, mr, hits_at
from utils.embeddings import sort_embeddings
from utils.utils_mytorch import mt_save_dir
from loops.sampler import MultiClassSampler
from loops.loops import training_loop_gcn

# Clamp the randomness
np.random.seed(42)
random.seed(42)
torch.manual_seed(132)


def merge_args(all_config: dict, gcn_config: dict, wn_config: dict, raw_args: list) -> dict:
    """
        Convert raw command line arguments, and merge them into the existing configuration dict.

        First, make a copy of the default arguments

        For each raw arg, check if the argument already exists in the config
            e.g. BATCH_SIZE -> find the default type of this argument in existing config (eg int),
                and convert the value to it.
            e.g. WN_MODE -> the prefix `wn` suggests that this is a wordnet arg.
                Look for its type in the wordnet config
            e.g. GCN_LAYERS -> the prefix `gcn` suggests its a STARE arg.
                Look for its type in the gcnconfig
            e.g. XYZ -> not in the dict, add the value as it is.

            Write the updated arg key and arg value to the corresponding dict (overwrite)

        Finally, merge the overwritten/modified config dicts into one dict

        **USAGE**:
            `python run.py a b c d e f` -> {a:b, c:d, e:f}
                where a, c, and e can be prefixed with WN_ or GCN_ to
                correspond to WordNet or GCN arguments respectively.
    """
    # Copy org dicts
    all_config = all_config.copy()
    gcn_config = gcn_config.copy()
    wn_config = wn_config.copy()

    # Use MyTorch to parse raw args into a dict where {a,b,c,d} turn to -> {a:b, c:d}
    parsed_args = parse_args(raw_args)
    print(parsed_args)

    # Superimpose this on default config
    for key, value in parsed_args.items():
        # If its a generic arg
        if key in all_config.keys():
            preset_val = all_config[key.upper()]
            if preset_val is not None:
                needed_type = type(preset_val)
                all_config[key.upper()] = needed_type(value)
            else:
                all_config[key.upper()] = value

        # If its a stare arg
        elif key.lower().startswith('gcn_') and key[4:] in gcn_config:
            preset_val = gcn_config[key[4:].upper()]
            if preset_val is not None:
                needed_type = type(preset_val)
                gcn_config[key[4:].upper()] = needed_type(value)
            else:
                gcn_config[key[4:].upper()] = value

        # If its a wordnet arg
        elif key.lower().startswith('wn_') and key[3:] in wn_config:
            preset_val = wn_config[key[3:].upper()]
            if preset_val is not None:
                needed_type = type(preset_val)
                wn_config[key[3:].upper()] = needed_type(value)
            else:
                wn_config[key[3:].upper()] = value

        else:
            all_config[key.upper()] = value

    # Merge modified dicts back to main config
    all_config['GCNARGS'] = gcn_config
    all_config['WNARGS'] = wn_config

    # Throw it back
    return all_config


if __name__ == "__main__":

    # Prepare one configuration dict
    config = merge_args(all_config=ARGS, gcn_config=GCN_ARGS,
                        wn_config=WN_ARGS, raw_args=sys.argv[1:])

    """
        Custom Sanity Checks
    """
    # If we're corrupting something apart from S and O
    if max(config['CORRUPTION_POSITIONS']) > 2:
        assert config['ENT_POS_FILTERED'] is False, \
            f"Since we're corrupting objects at pos. {config['CORRUPTION_POSITIONS']}, " \
            f"You must allow including entities which appear exclusively in qualifiers, too!"

    """
        Loading and preparing data
        
        Typically, sending the config dict, and executing the returned function gives us data,
        in the form of
            -> train_data (list of list of 43 / 5 or 3 elements)
            -> valid_data
            -> test_data
            -> n_entities (an integer)
            -> n_relations (an integer)
            -> ent2id (dictionary to interpret the data above, if needed)
            -> rel2id
    """
    data: dict = KGDataManager.load(config=config)()
    # Annotate the vars for sanity's sake
    train_data: List[List[int]]
    valid_data: List[List[int]]
    test_data: List[List[int]]
    n_entities: int
    n_relations: int
    entoid: Dict[str, int]
    reltoid: Dict[str, int]

    # Break down the data
    try:
        train_data, valid_data, test_data, n_entities, n_relations, entoid, reltoid = data.values()
    except ValueError:
        raise ValueError(f"Honey I broke the loader for {config['DATASET']}")

    config['NUM_ENTITIES'] = n_entities
    config['NUM_RELATIONS'] = n_relations

    # If TRIM is set (for faster dev cycle), trim train to 10* batch size, valid to 5*batch size
    if config['TRIM']:
        train_data = train_data[:10*config['BATCH_SIZE']]
        valid_data = valid_data[:5*config['BATCH_SIZE']]

    # Exclude entities which don't appear in the dataset. E.g. entity nr. 455 may never appear.
    # always off for wikipeople and jf17k
    if config['DATASET'] == 'jf17k' or config['DATASET'] == 'wikipeople':
        config['ENT_POS_FILTERED'] = False

    if config['ENT_POS_FILTERED']:
        ent_excluded_from_corr = KGDataManager.gather_missing_entities(
            data=train_data + valid_data + test_data,
            positions=config['CORRUPTION_POSITIONS'],
            n_ents=n_entities)
    else:
        ent_excluded_from_corr = [0]

    """
     However, when we want to run a GCN based model, we also work with
            COO representations of triples and qualifiers.
    
            In this case, for each split: [train, valid, test], we return
            -> edge_index (2 x n) matrix with [subject_ent, object_ent] as each row.
            -> edge_type (n) array with [relation] corresponding to sub, obj above
            -> quals (3 x nQ) matrix where columns represent quals [qr, qv, k] for each k-th edge that has quals
    
        So here, train_data_gcn will be a dict containing these ndarrays.
    """

    if config['MODEL_NAME'].lower().startswith('stare'):
        # Replace the data with their graph repr formats
        if config['GCNARGS']['QUAL_REPR'] == 'full':
            if config['USE_TEST']:
                train_data_gcn = KGDataManager.get_graph_repr(train_data + valid_data, config)
            else:
                train_data_gcn = KGDataManager.get_graph_repr(train_data, config)
        elif config['GCNARGS']['QUAL_REPR'] == 'sparse':
            if config['USE_TEST']:
                train_data_gcn = KGDataManager.get_alternative_graph_repr(train_data + valid_data,
                                                                          config)
            else:
                train_data_gcn = KGDataManager.get_alternative_graph_repr(train_data, config)
        else:
            print("Supported QUAL_REPR are `full` or `sparse`")
            raise NotImplementedError

        # add reciprocals to the train data
        reci = KGDataManager.add_reciprocals(train_data, config)
        train_data.extend(reci)
        reci_valid = KGDataManager.add_reciprocals(valid_data, config)
        valid_data.extend(reci_valid)
        reci_test = KGDataManager.add_reciprocals(test_data, config)
        test_data.extend(reci_test)
    else:
        train_data_gcn, valid_data_gcn, test_data_gcn = None, None, None

    print(f"Training on {n_entities} entities")

    """
        Make the model.
    """

    # Set the device
    config['DEVICE'] = torch.device(config['DEVICE'])

    # Initialising the model with embeddings
    if config['WNARGS']['INITIAL']:

        # Pull the appropriate node embeddings from dict
        node_init_loc = LOC.synset_embedding_dir / config['DATASET'] / (config['WNARGS']['INITIAL'] + '.pkl')
        assert node_init_loc.exists(), f"No node embeddings found at {node_init_loc}"

        with node_init_loc.open('rb') as node_inits_f:
            node_inits_raw: Dict[str, np.ndarray] = pickle.load(node_inits_f)

        node_inits: Optional[np.ndarray] = sort_embeddings(node_inits_raw, entoid)
    else:
        node_inits: Optional[np.ndarray] = None

    # if config['MODEL_NAME'].lower().startswith('stare'):
    #     if config['MODEL_NAME'].lower().endswith('trans_baseline'):
    #         model = TransformerBaseline(config)
    #     elif config['MODEL_NAME'].lower().endswith('triple_baseline'):
    #         # Sampler should not be qualifier aware.
    #         assert config['SAMPLER_W_QUALIFIERS'] is True, "only works for qual-aware encoder"
    #         model = StarE_Transformer_TripleBaseline(train_data_gcn, config)
    #     elif config['MODEL_NAME'].lower().endswith('stats_baseline'):
    #         if config['SAMPLER_W_QUALIFIERS']:
    #             model = Transformer_Statements(config)
    #         else:
    #             raise NotImplementedError
    #     elif config['MODEL_NAME'].lower().endswith('convkb'):
    #         if config['SAMPLER_W_QUALIFIERS']:
    #             print(f"ConvKB will use "
    #                   f"{(config['MAX_QPAIRS'] - 1, config['GCNARGS']['KERNEL_SZ'])} "
    #                   f"kernel. Otherwise change KERNEL_SZ param. Standard is 1")
    #             model = StarE_ConvKB_Statement(train_data_gcn, config)
    #         else:
    #             raise NotImplementedError
    #     elif config['MODEL_NAME'].lower().endswith('transformer'):
    #         if config['SAMPLER_W_QUALIFIERS']:
    #             if 'objectmask' in config['MODEL_NAME']:
    #                 model = StarE_ObjectMask_Transformer(train_data_gcn, config)
    #             else:
    #                 model = StarE_Transformer(train_data_gcn, config)
    #         else:
    #             model = StarETransformerTriples(train_data_gcn, config)
    #
    #     else:
    #         raise BadParameters(f"Unknown Model Name {config['MODEL_NAME']}")
    # else:
    #     raise BadParameters(f"Unknown Model Name {config['MODEL_NAME']}")
    if config['MODEL_NAME'].lower().startswith('stare') and config['MODEL_NAME'].lower().endswith('transformer'):
        if config['SAMPLER_W_QUALIFIERS']:
            raise BadParameters(f"No way to use SAMPLER_W_QUALIFIERS for {config['MODEL_NAME']}")
        else:
            model = StarETransformerTriples(train_data_gcn, config, node_inits=node_inits)
    else:
        raise BadParameters(f"Unknown Model Name {config['MODEL_NAME']}")

    model.to(config['DEVICE'])
    print("Model params: ", sum([param.nelement() for param in model.parameters()]))

    # Saving stuff
    # Specify the directory where to save things, and the content to be saved along with the model
    if config['SAVE']:

        # Find the directory based on the dataset and the model
        savedir = Path(f"./models/{config['DATASET']}/{config['MODEL_NAME']}")
        savedir.mkdir(parents=True, exist_ok=True)

        # Now, find which dir to pick. e.g. ls ./models/wn_synset/stare_transformer -> 0, 1, 2 ...
        if not config['RESUME']:
            # Resume flag is false -> create a new dir in savedir and return it
            savedir = mt_save_dir(savedir, _newdir=True)
        elif config['RESUME'] and int(config['RESUME_NM']) >= 0:
            # If Resume flag is true, and a resume dir is specified
            savedir = savedir / str(config['RESUME_NM'])
            # Check if the directory exists. If not, raise assertion error
            assert savedir.exists(), f"No sub folder {config['RESUME_NM']} in {savedir.parent}. Cannot resume from it!"

            # Pull the WANDB ID if WANDB flag is TRUE
            if config['WANDB']:
                config['WANDBID'] = get_saved_wandb_id(savedir)
        elif config['RESUME'] and int(config['RESUME_NM']) < 0:
            # If Resume flag is true, but no resume dir is specified, pick the last one
            savedir = mt_save_dir(savedir, _newdir=False)

            # Pull the WANDB ID if WANDB flag is TRUE
            if config['WANDB']:
                config['WANDBID'] = get_saved_wandb_id(savedir)
        else:
            raise AssertionError("Honey I broke the model savedir creation code.")

        # Create objects to save
        json_stuff = [tosave(obj=config, fname='config.json'),
                      tosave(obj=entoid, fname='entoid.json'),
                      tosave(obj=reltoid, fname='reltoid.json')]
        save_content = {'model': model, 'json': json_stuff}
    else:
        savedir, save_content = None, None

    # Configuring the optimizer
    if config['OPTIMIZER'] == 'sgd':
        optimizer = torch.optim.SGD(model.parameters(), lr=config['LEARNING_RATE'])
    elif config['OPTIMIZER'] == 'adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=config['LEARNING_RATE'])
    else:
        print("Unexpected optimizer, we support `sgd` or `adam` at the moment")
        raise NotImplementedError

    # Configuring WandB
    if config['WANDB']:
        wandb_id = wandb.util.generate_id() if config['WANDBID'] == '' else config['WANDBID']
        config['WANDBID'] = wandb_id
        wandb.init(project="wordnet-embeddings", id=wandb_id, entity="magnet", resume="allow")
        wandb.config.update(config, allow_val_change=True)
    """
        Prepare test benches.
        
            When computing train accuracy (`ev_tr_data`), we wish to use all the other data 
                to avoid generating true triples during corruption. 
            Similarly, when computing test accuracy, we index train and valid splits 
                to avoid generating negative triples.
    """
    if config['USE_TEST']:
        ev_vl_data = {'index': combine_splits(train_data, valid_data), 'eval': combine_splits(test_data)}
        ev_tr_data = {'index': combine_splits(valid_data, test_data), 'eval': combine_splits(train_data)}
        tr_data = {'train': combine_splits(train_data, valid_data), 'valid': ev_vl_data['eval']}
    else:
        ev_vl_data = {'index': combine_splits(train_data, test_data), 'eval': combine_splits(valid_data)}
        ev_tr_data = {'index': combine_splits(valid_data, test_data), 'eval': combine_splits(train_data)}
        tr_data = {'train': combine_splits(train_data), 'valid': ev_vl_data['eval']}

    eval_metrics = [acc, mrr, mr, partial(hits_at, k=3),
                    partial(hits_at, k=5), partial(hits_at, k=10)]

    evaluation_valid = None
    evaluation_train = None

    # The args to use if we're training w default stuff
    args = {
        "epochs": config['EPOCHS'],
        "data": tr_data,
        "opt": optimizer,
        "train_fn": model,
        # "neg_generator": Corruption(n=n_entities, excluding=[0], position=list(range(0, config['MAX_QPAIRS'], 2))),
        "device": config['DEVICE'],
        "data_fn": None,
        # "eval_fn_trn": evaluate_pointwise,
        "val_testbench": None,
        "trn_testbench": None,
        "eval_every": config['EVAL_EVERY'],
        "log_wandb": config['WANDB'],
        "run_trn_testbench": False,
        "savedir": savedir,
        "save_content": save_content,
        "qualifier_aware": config['SAMPLER_W_QUALIFIERS'],
        "grad_clipping": config['GRAD_CLIPPING'],
        "scheduler": None,
        "resume": config['RESUME']
    }

    if config['MODEL_NAME'].lower().startswith('stare'):
        training_loop = training_loop_gcn
        sampler = MultiClassSampler(data=args['data']['train'],
                                    n_entities=config['NUM_ENTITIES'],
                                    lbl_smooth=config['LABEL_SMOOTHING'],
                                    bs=config['BATCH_SIZE'],
                                    with_q=config['SAMPLER_W_QUALIFIERS'])
        evaluation_valid = EvaluationBenchGNNMultiClass(ev_vl_data, model, bs=config['BATCH_SIZE'],
                                                        metrics=eval_metrics,
                                                        filtered=True, n_ents=n_entities,
                                                        excluding_entities=ent_excluded_from_corr,
                                                        positions=config.get(
                                                            'CORRUPTION_POSITIONS', None),
                                                        config=config)
        args['data_fn'] = sampler.reset
        args['val_testbench'] = evaluation_valid.run
        args['trn_testbench'] = None
        if config['LR_SCHEDULER']:
            scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.95)
            args['scheduler'] = scheduler
    else:
        raise BadParameters(f"Model name not understood: {config['MODEL_NAME']}")

    traces = training_loop(**args)

    with open('traces.pkl', 'wb+') as f:
        pickle.dump(traces, f)
