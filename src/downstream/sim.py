"""
    Authored: Priyansh Trivedi, Jan 2021

    This file is responsible for using pretrained embeddings and
    computing various methods of similarity between word pairs
"""
import nltk
nltk.data.path.append("/home/priyansh/Dev/perm/nltk/nltk_data")  # TODO: look for a permanent fix

import json
import torch
import numpy as np
from pathlib import Path
from functools import partial
from nltk.corpus import wordnet
from typing import Dict, Callable

# Local Imports
from config import LOCATIONS as LOC, NLP_ARGS
from data_loaders import nlp as datasets
from utils.nlp import getvec, KNOWN_SIMILARITY as sim_fns
from utils.metrics import spearman

"""
    Functions to Select Synsets
"""


def _synsets_for_words_all_(wa: str, wb: str, getvec: Callable) -> (torch.Tensor, torch.Tensor):
    """
        Get a pair of lists of all synsets for a given pair of words
        Get a mean of all vectors

        Selection Strategy:
            - get all synsets for word_a, get all synsets for word_b
    """
    sa = [synset.name() for synset in wordnet.synsets(wa)]
    sb = [synset.name() for synset in wordnet.synsets(wb)]

    vas = torch.cat([getvec(x).unsqueeze(1) for x in sa], dim=1)
    vbs = torch.cat([getvec(x).unsqueeze(1) for x in sb], dim=1)

    # Compute the mean for each list of vectors
    va, vb = torch.mean(vas, dim=1), torch.mean(vbs, dim=1)

    return va, vb


def _synsets_for_words_max_(wa: str, wb: str, getvec: Callable, sim_fn: Callable) -> (torch.Tensor, torch.Tensor):
    """
        Get a single pair of synsets for a given pair of words.

        Selection Strategy:
            - get all synsets for word_a, get all synsets for word_b
            - for all pairs (n,m), compute the similarity

    """
    sa = [synset.name() for synset in wordnet.synsets(wa)]
    sb = [synset.name() for synset in wordnet.synsets(wb)]

    vas = torch.cat([getvec(x).unsqueeze(1) for x in sa], dim=1)
    vbs = torch.cat([getvec(x).unsqueeze(1) for x in sb], dim=1)

    # Compute the n * m matrix of similarity b/w vas, vbs
    # sim = torch.matmul(vas.transpose(0, 1), vbs)  # Only works when sim_fn is cos
    sim = torch.zeros(vas.shape[1], vbs.shape[1], dtype=torch.float)
    for i in range(vas.shape[1]):
        for j in range(vbs.shape[1]):
            sim[i,j] = sim_fn(vas[:, i], vbs[:, j])

    # Get the maximum index
    max_a, max_b = ((sim == torch.max(sim)) * 1.0).nonzero().tolist()[0]

    return getvec(sa[max_a]), getvec(sb[max_b])


def _load_kge_(loc: Path, device: str = 'cpu') -> (torch.Tensor, Dict[str, int]):
    assert (loc / 'entvecs.torch').exists(), f"No embeddings found at {loc}"
    assert (loc / 'entoid.json').exists(), f"No vocab found at {loc}"

    embeddings = torch.load(loc / 'entvecs.torch').to(device)
    with (loc / 'entoid.json').open('r', encoding='utf8') as f:
        entoid: Dict[str, int] = json.load(f)

    # Sanity Checks
    assert len(entoid) == embeddings.shape[0], f"Shape Mismatch: " \
                                               f"Voc: {len(entoid)}, Emb: {embeddings.shape[0]}"

    return embeddings, entoid


def yield_word_vector_converter(embeddings: torch.Tensor, entoid: Dict[str, int], args: dict, sim_fn: Callable) \
        -> Callable:
    """ Returns a callable that can return one vector corresponding to a word pair. """
    if args['KG_FORM'].endswith('synset'):
        synset_encoder = partial(getvec, e2id=entoid, id2vec=embeddings)

        if args['STRATEGY'] == 'all':
            return partial(_synsets_for_words_all_, getvec=synset_encoder)
        elif args['STRATEGY'] == 'max':
            return partial(_synsets_for_words_max_, getvec=synset_encoder, sim_fn=sim_fn)
    else:
        raise NotImplementedError("No other form of WN KG but synset is implemented")


def _run_(args: dict):
    """
        Assume arguments are already parsed, and are sanity checked

        1. Load the dataset
        1. Load a pretrained model
        1. For each test instance
            1. Get synsets based on a particular

    """
    dataset = datasets._load_sl999_()  # TODO: Make generic based on args
    embeddings, entoid = _load_kge_(loc=LOC.models / args['KG_FORM'] / args['KG_MODEL'] / args['KG_MODELNM'],
                                    device=args['DEVICE'])

    true = np.asarray(dataset['test']['Y'], dtype=np.float)
    pred = np.zeros_like(true, dtype=np.float)

    # Find the function used to compose the word embeddings
    sim_fn = sim_fns[args['SIMILARITY']]
    enc_fn = yield_word_vector_converter(embeddings=embeddings, entoid=entoid, args=args, sim_fn=sim_fn)

    for i, (x, y) in enumerate(zip(dataset['test']['X'], dataset['test']['Y'])):
        wa, wb = x
        va, vb = enc_fn(wa, wb)
        pred[i] = sim_fn(va, vb)

    # Now we have both true and pred, lets compute some metrics
    spearman_r, spearman_p = spearman(true, pred)

    # TODO: what other matrices
    return spearman_r, spearman_p


if __name__ == "__main__":
    NLP_ARGS['STRATEGY'] = 'all'
    print(_run_(NLP_ARGS))
