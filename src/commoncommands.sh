# DO NOT RUN THIS FILE. I'm simply storing some commonly used commands here!

oarsub -l gpu=1,walltime=3 "source ~/.bashrc;conda activate main;cd ~/word/wordnet-embeddings;python src/run.py WANDB True SAMPLER_W_QUALIFIERS False DEVICE cuda SAVE True EVAL_EVERY 4 EPOCHS 100 DATASET wn_synset MODEL_NAME stare_transformer"
