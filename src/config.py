import sys
from pathlib import Path
from typing import List

from loguru import logger
from mytorch.utils.goodies import FancyDict

KGE_DATASETS: List[str] = ['wn_synset', 'wn_all', 'wn_lex', 'wn_synset_mined', 'wn_synset_unirel']
NLP_DATASETS: List[str] = ['sl999']

"""
    ROOT_LOC = '..' if code is called from src
    ROOT_LOC = '.' if code is called from project root
    
    Prefixing with this ensures code works from both places of being called. 
    Python path management is hell.
"""
ROOT_LOC: Path = Path('..') if str(Path().cwd()).split('/')[-1] == 'src' else Path('.')
LOCATIONS: FancyDict = FancyDict(
    raw=ROOT_LOC / Path('data/raw'),
    parsed=ROOT_LOC / Path('data/parsed'),
    cleaned=ROOT_LOC / Path('data/clean'),
    manual=ROOT_LOC / Path('data/manual'),
    wn_schema=ROOT_LOC / Path('data/manual/wn_schema.json'),
    logging_dir=ROOT_LOC / Path('logs'),
    models=ROOT_LOC / Path('models'),
    wordtorev=ROOT_LOC / Path('models/nlp/word2vec/GoogleNews-vectors-negative300.bin'),
    synset_embedding_dir=ROOT_LOC / Path('data/manual/node-inits'),
    mined_simple_wikitext=ROOT_LOC / Path('data/raw/mined-simple-wikitext')
)

ARGS = {
    'BATCH_SIZE': 128,
    'DATASET': 'wn_synset',
    'DEVICE': 'cpu',
    'EMBEDDING_DIM': 200,
    'ENT_POS_FILTERED': True,
    'EPOCHS': 401,
    'EVAL_EVERY': 5,
    'LEARNING_RATE': 0.0001,

    # If set True, this reduces the train and test datasets to 10*BATCH_SIZE to make things faster
    'TRIM': False,

    # If 3: only triples. If -1, then statements (len capped using MAX_QPAIRS)
    'STATEMENT_LEN': 3,

    # STATEMENT_LEN decides tri/qu/statements but this hardcaps the len.
    'MAX_QPAIRS': 15,

    # Which model to init
    'MODEL_NAME': 'stare_transformer',

    # While evaluation, where should we replace resources to create negative pairs.
    # e.g. [0, 2]: subject and object; [0]: only subject ...
    'CORRUPTION_POSITIONS': [0, 2],

    # # not used for now
    # 'MARGIN_LOSS': 5,
    # 'NARY_EVAL': False,
    # 'NEGATIVE_SAMPLING_PROBS': [0.3, 0.0, 0.2, 0.5],
    # 'NEGATIVE_SAMPLING_TIMES': 10,
    # 'NORM_FOR_NORMALIZATION_OF_ENTITIES': 2,
    # 'NORM_FOR_NORMALIZATION_OF_RELATIONS': 2,
    # 'NUM_FILTER': 5,
    # 'PROJECT_QUALIFIERS': False,
    # 'PRETRAINED_DIRNUM': '',
    # 'RUN_TESTBENCH_ON_TRAIN': False,
    # 'SAVE': False,
    # 'SELF_ATTENTION': 0,
    # 'SCORING_FUNCTION_NORM': 1,

    # important args
    'SAVE': False,

    # If True, then we eval on testset, else we eval on validation set
    'USE_TEST': False,

    # Log things in WANDB
    'WANDB': False,

    # IGNORE. Set automatically
    'WANDBID': "",
    'LABEL_SMOOTHING': 0.1,
    'SAMPLER_W_QUALIFIERS': True,
    'OPTIMIZER': 'adam',
    'CLEANED_DATASET': False,  # should be false for WikiPeople and JF17K for their original data

    'GRAD_CLIPPING': True,
    'LR_SCHEDULER': True,

    # Args which indicate if we are to train a new model, or pull a old (state dict) from disk and resume training
    'RESUME': False,

    # Resuming path goes like ./models/DATASET/MODEL_NAME/int(RESUME_NM), e.g. ./models/wn_synset/stare_transformer/1
    # When set to -1, the LAST folder inside ./models/DATASET/MODEL_NAME is used.
    'RESUME_NM': -1
}

# Arguments specific to wordnet
WN_ARGS = {
    # NOT BEING USED. IT IS INFERRED FROM config['DATASET']
    'MODE': 'synset',

    # Can be things like wordtovec-gloss, wordtovec-lemma-avg, wordtovec-synset ...
    'INITIAL': '',

    # This flag includes every statement in the train set, so we get good embeddings for everything.
    # Keep it turned off if you're doing experiments on the actual task and not using it for something else.
    'TRAIN_ON_TEST': False
}

GCN_ARGS = {
    'LAYERS': 2,
    'N_BASES': 0,
    'GCN_DIM': 200,
    'GCN_DROP': 0.1,
    'HID_DROP': 0.3,
    'BIAS': False,
    'OPN': 'rotate',
    'TRIPLE_QUAL_WEIGHT': 0.8,
    'QUAL_AGGREGATE': 'sum',  # or concat or mul
    'QUAL_OPN': 'rotate',
    'QUAL_N': 'sum',  # or mean
    'SUBBATCH': 0,
    'QUAL_REPR': 'sparse',  # sparse or full. Warning: full is 10x slower
    'ATTENTION': False,
    'ATTENTION_HEADS': 4,
    'ATTENTION_SLOPE': 0.2,
    'ATTENTION_DROP': 0.1,
    'HID_DROP2': 0.1,

    # For ConvE Only
    'FEAT_DROP': 0.3,
    'N_FILTERS': 200,
    'KERNEL_SZ': 7,
    'K_W': 10,
    'K_H': 20,

    # For Transformer
    'T_LAYERS': 2,
    'T_N_HEADS': 4,
    'T_HIDDEN': 512,
    'POSITIONAL': True,
    'POS_OPTION': 'default',
    'TIME': False,
    'POOLING': 'avg'
}

ARGS['GCNARGS'] = GCN_ARGS
ARGS['WNARGS'] = WN_ARGS

NLP_ARGS: dict = {
    'DATASET': 'sl999',
    'DEVICE': 'cpu',
    'SIMILARITY': 'cosine',
    'STRATEGY': 'all',
    'KG_FORM': 'wn_synset',
    'KG_MODEL': 'stare_transformer',
    'KG_MODELNM': '1'
}


def initialize_logger(output_dir: Path = LOCATIONS.logging_dir, caller: str = '__main__') -> None:
    """
        Configure the logger to direct it where we want.

    :param output_dir: The pathlib.Path instance pointing to the dir where we want our logs.
    :param caller: The primary calling function (ignore).
    :return: None
    """
    logger.add(output_dir / "info.log", level='INFO', encoding='UTF8',
               colorize=True, backtrace=False, diagnose=False)
    logger.add(output_dir / "debug.log", level='DEBUG', encoding='UTF8',
               colorize=True, backtrace=True, diagnose=True)
    logger.add(output_dir / "error.log", level='ERROR', encoding='UTF8',
               colorize=True, backtrace=True, diagnose=True)
    logger.add(sys.stderr, level='INFO', colorize=True,
               backtrace=False, diagnose=False)
    logger.info(f"------------ Called by: {caller} ------------")
