""" A collection of functions to be used for NLP tasks """
from typing import Callable, Dict

import torch
import torch.nn as nn

cos = nn.CosineSimilarity(dim=0)


def getvec(w: str, e2id: Dict[str, int], id2vec: torch.Tensor) -> torch.Tensor:
    return id2vec[e2id.get(w, 0)]


"""
    Some functions which compute similarity between two vectors
"""


def cosine(va: torch.Tensor, vb: torch.Tensor) -> float:
    """ va.vb / ||va|| * ||vb||"""
    return cos(va, vb).tolist()


def tanimoto(va: torch.Tensor, vb: torch.Tensor) -> float:
    """ va.vb / ||va||^2 + ||vb||^2 - va.vb """
    return (
            va.dot(vb)
            / (torch.sum(va * va).sqrt() + torch.sum(vb * vb).srqrt() - va.dot(vb))
    ).tolist()


KNOWN_SIMILARITY: Dict[str, Callable] = {'cosine': cosine, 'tanimoto': tanimoto}
