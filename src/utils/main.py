"""
    Some things needed across the board
    NOTE: This is top level import, i.e., it MUST NOT IMPORT config
"""
import json
from collections import namedtuple
from pathlib import Path
from typing import List, Union

import numpy as np
import torch


# edit: works now | Does not work if UTILS and CONFIG are both imported somewhere, which is almost always.
# assert 'config' not in sys.modules, "You imported config. Lord knows what happens next!"


class UnknownSynsetEmbeddingMethod(Exception):
    pass


Quint = namedtuple('Quint', 's p o qp qe')


def masked_softmax(x, m=None, dim=-1):
    """
    Softmax with mask
    :param x:
    :param m:
    :param dim:
    :return:
    """
    if m is not None:
        m = m.float()
        x = x * m
    e_x = torch.exp(x - torch.max(x, dim=dim, keepdim=True)[0])
    if m is not None:
        e_x = e_x * m
    softmax = e_x / (torch.sum(e_x, dim=dim, keepdim=True) + 1e-6)
    return softmax


def combine_splits(*args: Union[np.ndarray, list, tuple]):
    """
        Used to semi-intelligently combine data splits

        Case A)
            args is a single element, an ndarray. Return as is.
        Case B)
            args are multiple ndarray. Numpy concat them.
        Case C)
            args is a single dict. Return as is.
        Case D)
            args is multiple dicts. Concat individual elements

    :param args: (see above)
    :return: A nd array or a dict
    """

    # Case A, C
    if len(args) == 1 and type(args[0]) is not dict:
        return np.array(args[0])

    if len(args) == 1 and type(args) is dict:
        return args

    # Case B
    if type(args) is tuple and (type(args[0]) is np.ndarray or type(args[0]) is list):
        # Expected shape will be a x n, b x n. Simple concat will do.
        return np.concatenate(args)

    # Case D
    if type(args) is tuple and type(args[0]) is dict:
        # noinspection PyUnresolvedReferences
        keys = args[0].keys()
        combined = {}
        for k in keys:
            combined[k] = np.concatenate([arg[k] for arg in args], dim=-1)
        return combined


def dump_to_disk(data: List[List[str]], path: Path):
    """
        Expect a list of list of strings.
        Each element of the top list is a unique triple/statement
    """

    # Check if the path exists or not
    assert not path.is_dir(), f"Expected path to file, not to a directory: {path}"

    try:
        assert path.exists()
    except AssertionError:
        if not path.parent.exists():
            path.parent.mkdir(parents=True)
            print(f"Created a new directory: {path.parent}")
        path.touch()
        # TODO: move to logs
        print(f"Created a new file: {path}")

    dumping_data: List[str] = []
    for datum in data:
        dumping_data.append(','.join([x.strip().replace(' ', '_') for x in datum]))

    # Push it to disk
    with open(path, mode='w+', encoding='utf8') as f:
        f.writelines('\n'.join(dumping_data))


def get_saved_wandb_id(loc: Path):

    with (loc / 'config.json' ).open('r', encoding='utf8') as f:
        config = json.load(f)

    return config['WANDBID']
