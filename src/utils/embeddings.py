"""
    This file is aimed to generate embeddings for all [synsets, ...] WordNet.

    # Usage
        python src/utils/embeddings.py wordtovec wn_synset lemma-avg gloss synset

        First arg: embedding source to use (e.g. wordtovec, bert etc)
        Second arg: what to embed (synsets, lemmas etc)
        Next args: all the different methods by which we can use the embedding source

    # Existing Sources:

    1. Word2Vec skip-gram trained on Google News, 300D
        1. Average vector of all lemmas in a synset
        1. Average vector of gloss
        1. One vector of the synset name
    1. TODO:  BERT of lemma names, TODO: SentenceBert of gloss/examples

    # Structure of this file: class EmbedSynset

    1. Init function which loads the relevant embedding method
    1. A function which iterates over all synsets and collect vectors: `run`
    1. A function which embeds one synset `_embed_synset_`
    1. Functions which interface every embedding method
"""
import pickle
import sys
from typing import List, Dict, Callable, Set

import nltk
import numpy as np
from gensim.models import KeyedVectors

nltk.data.path.append("/home/priyansh/Dev/perm/nltk/nltk_data")  # TODO: look for a permanent fix
from nltk.corpus import wordnet as wn

# Local imports
from config import LOCATIONS as LOC
from utils.main import UnknownSynsetEmbeddingMethod


class EmbedSynset:
    """ Create unique embeddings for each mentioned method, dump them all to disk uniquely"""

    def __init__(self, source: str, methods: List[str], wn_format: str = 'wn_synset'):

        # Sanity Checks
        if wn_format == 'wn_synset':
            ...
        else:
            raise NotImplementedError(f"No way to embed {wn_format}")

        self.methods: List[str] = methods
        self.data: Dict[str, np.ndarray] = {}
        self.unks: Set[str] = set()
        self.embedding_nm: str = source
        self.wn_synset: str = wn_format

        # Load vectors source
        if self.embedding_nm == 'wordtovec':
            self.embedding_raw = KeyedVectors.load_word2vec_format(LOC.wordtovec, binary=True)
            self.embedding: Callable = self._wordtovec_
        else:
            raise NotImplementedError(f"No way to use {self.embedding_nm} to get embeddings")

        # Run the code for every embedding method
        for method in self.methods:
            self.run(method)

    def _wordtovec_(self, tokens: List[str]):
        """ Input is list of tokens, output is a list of ndarrays, and a set of strings"""
        vecs: List[np.ndarray] = []
        unks: List[str] = []
        for token in tokens:
            try:
                vecs.append(self.embedding_raw[token])
            except KeyError:
                unks.append(token)
                vecs.append(self.embedding_raw['UNK'])

        return vecs, set(unks)

    def run(self, method: str):
        """
            1. Iterate over all synsets, create a vector for each synset
            1. Dump it to the location as a dict (pickled)
        """
        for synset in wn.all_synsets():
            vec, unk = self._embed_synset_(synset, method)
            self.unks.union(unk)
            self.data[synset.name()] = vec

        print(f"When embedding with {self.embedding_nm} using {method} we encounter {len(self.unks)} unknown tokens.")

        # Now to dump it to disk
        outputdir = LOC.synset_embedding_dir / self.wn_synset
        outputdir.mkdir(parents=True, exist_ok=True)
        with (outputdir / f"{self.embedding_nm}-{method}.pkl").open('wb+') as f:
            pickle.dump(self.data, f)

        # Reset data and unknowns
        self.data: Dict[str, np.ndarray] = {}
        self.unks: Set[str] = set()

    def _embed_synset_(self, synset: nltk.corpus.reader.wordnet.Synset, method: str) -> (np.ndarray, List[str]):
        """
            Give the method in str, synset in WN, and embeddings as a fn:
                embeddings(tokens: List[str]) -> unknowns as List[str], vectors (including unks) as np.ndarray

            The final output is a fixed dimensional vector (shape: (n,)) , and a list of unknown tokens
        """
        if method == 'lemma-avg':
            # Get all lemmas' name, and average the vectors
            toks = [x.name() for x in synset.lemmas()]
            vec, unks = self.embedding(toks)
            vec = np.mean([x.reshape(1, -1) for x in vec], axis=0).reshape(-1)
        elif method == 'lemma-weighted':
            raise NotImplementedError("This method will need a lot of work lol")
        elif method == 'gloss':
            toks = synset.definition().split()
            vec, unks = self.embedding(toks)
            vec = np.mean([x.reshape(1, -1) for x in vec], axis=0).reshape(-1)
        elif method == 'synset':
            toks = [synset.name().split('.')[0]]
            vec, unks = self.embedding(toks)
            vec = np.mean([x.reshape(1, -1) for x in vec], axis=0).reshape(-1)
        else:
            raise UnknownSynsetEmbeddingMethod(f"No method called {method}.")

        return vec, unks


def sort_embeddings(raw: Dict[str, np.ndarray], keytoid: Dict[str, int]) -> np.ndarray:
    """
        Sort the arrays in raw based on the order in keytoid. Return a (n, dim) array

        We iterate over the keytoid, NOT the raw
            because there might be some entities in raw which we don't want in our model

        Also number of nodes in the model corresponds to keytoid and not raw.
    """

    # Find out the embedding dim, dtype
    emb_dim: int = -1
    emb_dtype: str = ''
    for val in raw.values():
        emb_dim = val.shape[0]
        emb_dtype = str(val.dtype)
        break

    # Find out the number of resources
    num_res = len(keytoid)

    # Init a empty matrix
    output = np.zeros((num_res, emb_dim), dtype=emb_dtype)

    # Iterate over the raw dict, and get the matrix index of each key from keytoid
    for node_nm, node_id in keytoid.items():
        output[node_id]: np.ndarray = raw.get(node_nm, np.zeros_like(output[0]))

    return output


if __name__ == "__main__":

    args = sys.argv[1:]
    embeddings_source = args[0]
    what_to_embed = args[1]
    embedding_methods = args[2:]

    # args: wordtovec wn_synset gloss (last arg can change frequently)
    _ = EmbedSynset(source=embeddings_source, wn_format=what_to_embed, methods=embedding_methods)
