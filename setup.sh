pip install -r requirements.txt

# Make directories
mkdir data
mkdir
mkdir data/raw
mkdir data/clean
mkdir data/manual
mkdir data/parsed
mkdir -pv data/raw/sl999
mkdir -pv models/nlp/word2vec
mkdir data/raw/mined-simple-wikitext

# Install datasets
wget https://fh295.github.io/SimLex-999.zip -P data/raw/sl999
unzip data/raw/sl999/SimLex-999.zip -d data/raw/sl999
mv data/raw/sl999/SimLex-999/* data/raw/sl999

# Get word vectors
echo "Please download word2vec from https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing manually to models/nlp/word2vec and extract it"
